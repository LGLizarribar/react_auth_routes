import { Link, useHistory } from "react-router-dom";

const Home = (props) => {
    const history = useHistory();

    const redirectWithJS = () => {
        console.log('click redirect');

        history.push('/pepesito', {
            saludo: 'hola',
        });
    }

    return (
        <div>
            <h1>Bienvenido a nuestra web!</h1>
            <p>
                Puedes registrarte como usuario, loguearte en el sistema y
                desloguearte
            </p>
            <ul>
                <li>
                    <Link to="/login">Ir a Login</Link>
                </li>

                <li>
                    <Link to="/register">Ir a Registro</Link>
                </li>

                <li onClick={redirectWithJS}>Javascript Link to Pepesitoouuuhh</li>

                <li>
                    <Link to={{
                        pathname: "/pepesito",
                        state: { counter: 123 }
                    }}>
                        Link a Pepesito con etiqueta Link
                    </Link>
                </li>
            </ul>
        </div>
    );
};

export default Home;