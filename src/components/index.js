import RegisterForm from './RegisterForm/RegisterForm';
import LoginForm from './LoginForm/LoginForm';
import Navbar from './Navbar/Navbar';
import Home from './Home/Home';
import Pepesito from './Pepesito/Pepesito';

export {
    RegisterForm,
    LoginForm,
    Navbar,
    Home,
    Pepesito,
}
