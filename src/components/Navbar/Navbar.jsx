import { Link, withRouter } from 'react-router-dom';
import { logout } from '../../api/auth';
import './Navbar.scss';

const Navbar = (props) => {
    // const history = useHistory();

    const logoutUser = async () => {
        try {
            await logout();
            props.saveUser(null);
        } catch (error) {
            console.log('error', error);
        }
    };
    
    // console.log(props);

    return (
        <nav className="nav">
            <div>
                <Link to="/">
                    Upgrade Auth
                </Link>
            </div>
            {props.user && <div>
                <span className="nav__text">
                    Bienvenido de nuevo, {props.user.username}
                </span>
                <button onClick={logoutUser}>Logout</button>
            </div>}
        </nav>
    )
}

export default withRouter(Navbar);
